# Everything you need to start your next React (or any type of javascript) project.

## What it does

- #### Effectively manage browser cache:
    - It will create versoned assets (app.31f67230b8aec9e9f89a.css, app.31f67230b8aec9e9f89a.js, etc.). A hash will be attached to the file names for the versoning. 
    - HTML file will be created dynamically with the entries of the hashed js and css file names. Find a sample html below.
    - If there is any change in source code, the hash will change so the file names and the html. Now, new files will be seeked from server and old cached files will be invalidated automatically.

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>My App</title>
    <link href="app.31f67230b8aec9e9f89a.css" rel="stylesheet">
  </head>
  <body>
    <div id="app" />
    <script type="text/javascript" src="vendor.bundle.31f67230b8aec9e9f89a.js"></script>
    <script type="text/javascript" src="app.31f67230b8aec9e9f89a.js"></script>
  </body>
</html>
```

- #### Optimize and split minified files:
    - Will remove all duplicate modules to create a very small build file.
    - Will separate application specific js and vendor js. As we don't add libraries everyday, the vendor js ( which is effectively much larger than the application specific js) fewer time in user's machine and cached longer. Only the application js will be downloaded( only if there is any change in the source code). These changes will decrease page load time significantly.


- #### React Hot Reload and Hot Module Replacement( Feel sorry for GULP already):
    - Another magical feature of this stack. Consider a scenario where to test an UI feature you need to perform some interaction with the page ( click some buttons, navigate to a certain page or fill a big form) and then you need to make some changes in your code. How nice it will be if the changes appear in the browser instantly after you save the changes. How about you don't loose the Ui state you are in, you don't loose any data you filled in the form, you're not thrown back to initial view of the page. Yes, thats what we do in this stack with React Hot .


- #### ES6/ES7 ready:
    - It will compile your fancy ES-6, ES-7 and ES-all codes.


- #### Not specific to React:
    - You can use this build system as it is with any of your javascript projects with minimum or no modifications.

## Get started


```sh
git clone git@bitbucket.org:jyotirmaybanerjee/react_router_webpack_seed.git
```

```sh
cd react_router_webpack_seed && npm install
```

```sh
npm start
```

#### For production build

```sh
npm run build
```