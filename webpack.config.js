var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
var OpenBrowserPlugin = require('open-browser-webpack-plugin');

var BUILD_DIR = path.resolve(__dirname, 'dist');
var APP_DIR = path.resolve(__dirname, 'src/client/app');

function getEntrySources(sources) {
    if (process.env.NODE_ENV !== 'production') {
        sources.push('webpack/hot/only-dev-server');
    }
    return sources;
}

var config = {
  entry: {
    app: getEntrySources([
            APP_DIR + '/index.jsx'
        ]
    ),
    vendor: [
        'react',
        'react-dom',
        'react-router'
    ]
  },
  output: {
    path: BUILD_DIR,
    filename: '[name].[hash].js'
  },
  module : {
    loaders : [
      {
        test : /\.jsx?/,
        include : APP_DIR,
        loaders : ['react-hot', 'babel', 'eslint'],
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract("style-loader", "css-loader")
      },
      {
         test: /\.less$/,
         loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader")
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'My App',
      inject: 'body',
      template: 'src/client/index.tmpl'
    }),
    // new OpenBrowserPlugin({ url: 'http://localhost:8080' }),
    new ExtractTextPlugin("[name].[hash].css"),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.optimize.CommonsChunkPlugin("vendor", "vendor.bundle.[hash].js"),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new webpack.optimize.DedupePlugin()
  ]
};

module.exports = config;
