import React, {Component} from 'react';
import {render} from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';

import Nav from './components/nav/Nav.jsx';
import About from './components/about/About.jsx';
import Users from './components/users/Users.jsx';
import NoMatch from './components/404/404.jsx';

import './styles/index.less';

class App extends Component {
  render () {
    return (
      <div>
        <Nav />
        {this.props.children}
      </div>
    )
  }
}

render((
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <Route path="about" component={About}/>
        <Route path="users" component={Users}/>
        <Route path="*" component={NoMatch}/>
      </Route>
    </Router>),
    document.getElementById('app')
);
