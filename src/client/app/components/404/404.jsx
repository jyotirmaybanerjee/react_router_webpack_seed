import React, { Component } from 'react';

export default class NoMatch extends Component {
  render () {
    return <p> Page not found! </p>;
  }
}
