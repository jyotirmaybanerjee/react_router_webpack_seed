# ECMAScript 6

JavaScript is a superset of ECMAScript scripting language. ECMAScript forms the language base for JavaScript, JScript and ActionScript. In this tutorial series we will look at ECMAScript 6, a new version of ECMAScript providing a new set features and fixes to JavaScript.

ECMAScript6 is also called as “ES6″, “Harmony” and “ES.next”.
####Categorizing ES6 Features

All the new features of ES6 can be categorized in 7 categories. Here is the list of categories:

- Variables and Parameters
- Classes
- Functional JavaScript
- Build-in Objects
- Asynchronous JavaScript
- ECMAScript 6 Objects
- Modules

###ECMAScript 6 Compatibility

Kangax has created a ES6 compatibility table which lists the ES6 features and browsers which support or don’t support them.

You can also use caniuse.com to find the support of various ES6 features on various different browsers.
###Running ECMAScript 6 in an Incompatible Browser

If you are writing ES6 for your website on development phase then you can embed the Traceur compiler in your webpages which will compile the ES6 to simple browser supportable JavaScript code on the fly in the browser.

Here is how to embed Traceur in your website

```html
<!doctype html>
<html>
    <head>...</head>
    <body>
        ...

        <!-- Load Traceur Compiler -->
        <script src="https://google.github.io/traceur-compiler/bin/traceur.js"></script>
        <!-- Bootstrap.js finds script tags with type module and compiler them using the interfaces provided by traceur.js -->
        <script src="https://google.github.io/traceur-compiler/src/bootstrap.js"></script>
        <script type="module">
            //Place ES6 code here
        </script>
    </body>
</html>
```
On production site compiling ES6 to browser supportable JS on every page load can be a resource & time consuming task and can effect the site performance. Its recommend that you use Traceur’s node compiler to compile once for all time and embed the compiled JS in your pages.

If you don’t like this compile idea then you can use ES6 polyfills. Polyfill is not available for every ES6 feature.

[ES6 fiddle]

Now let’s see the features provided by ECMAScript 6:

### "let" Keyword
“let” keyword was introduced in ES6. It lets you define block scope(bracket scope) variables in JavaScript. Initially JavaScript only supported function scope and global scope variables.

Here is an example code

```javascript
if(true)
{
    let x = 12;
    alert(x); //alert's 12
}
alert(x); //x is undefined here
```

“let” keyword limits the variables accessibility upto a block, statement or expression.

### "const" Keyword
“const” keyword was introduced in ES6. It lets you define read only variables using JavaScript. Variables created using “const” are block scoped(or bracket scoped). Redeclaring a “const” variable in the same scope throws an error.

Here is an code example:
```javascript
const x = 12;
//an constant 'x' is already available in this scope therefore the below line throws an error when you are try to create a new x variable.
const x = 13;
if(true)
{
    //an constant 'x' is available in this scope but not defined in this scope therefore the below line will not throw error instead define a new "x" inside this scope.
    const x = 13;
   
    //here 'y' is available inside this scope not outside this scope
    const y = 11;
}
//here creating a new 'y' will not throw an error because no other 'y' is available in this scope(i.e., global scope)
const y = 12;
```

Just remember that in a scope you cannot redeclare or change value of an “const” variable if a variable with same name is already available for access in that scope.

### JavaScript Function Return Multiple Values
There were many different ways purposed by JavaScript developers for returning multiple values in an function. But ECMAScript 6 has come up with an easy way to do it.

Here is an code example of how to return multiple values using ES6:

```javascript
function function_name()
{
    return [1, 6, 7, 4, 8, 0]; //here we are storing variables in an array and returning the array
}

var q, w, e, r, t, y;

//Here we are using ES6's array destructuring feature to assign the returned values to variables.
//Here we are ignoring 2 and 4 array indexes
[q, w, , r, , y] = function_name();

alert(y);//y is 0
```

ES6 provides a array like syntax to assign multiple variables to values of array indexes. It also lets you ignore some array indexes.

### Default Function Arguments Values
Object destructing feature in ES6 allows us to provide default values to function arguments.

Object destructing provides an object creation like syntax to assign variables to object properties. Here is an example code:

```javascript
var object = {x: 12, y: 67};

//Here object's property names are matched with the global variables names for assigning.
var {y, x, z} = object;

alert(x); //x is 12

x = 34;
alert(x); //x is 34

alert(object.x); //object.x is 12

alert(z); //z is undefined
```

Let’s see how we can use this feature to create default function arguments

```javascript
//The default object will have equal sign instead of colon. Inside object will be as usual
function function_name({x = 12, y = 13} = {})
{
    alert(x); //x is 12
    alert(y); //y is 19
}

function_name({y: 19});
```

###“…” Operator
ES6 introduced “…” operator which is also called as spread operator. When “…” operator is applied on an array it expands the array into multiple variables in syntax wise. And when its applied to an function argument it makes the function argument behave like array of arguments.

We can use spread operator to take indefinite number of arguments.

Here is an example code of how to use this operator:

```javascript
//args variable is an array holding the passed function arguments
function function_one(...args)
{  
    console.log(args);
    console.log(args.length);
}

function_one(1, 4);
function_one(1, 4, 7);
function_one(1, 4, 7, 0);


function function_two(a, b, ...args)
{
    console.log(args);
    console.log(args.length);
}

//"args" holds only 7 and 9
function_two(1, 5, 7, 9);
```
Before ES6 introduced “…” operator developers used Array.prototype.slice.call to retrieve the extra passed arguments.

Here is an example code:

```javascript
//args variable is an array holding the function arguments
function function_one()
{  
    var args = Array.prototype.slice.call(arguments, function_one.length);

    console.log(args);
    console.log(args.length);
}

function_one(1, 4);
function_one(1, 4, 7);
function_one(1, 4, 7, 0);


function function_two(a, b)
{
    var args = Array.prototype.slice.call(arguments, function_two.length);

    console.log(args);
    console.log(args.length);
}

//"args" holds only 7 and 9
function_two(1, 5, 7, 9);
```

If we apply “…” to an array it expands it into multiple variables syntax wise. Here is an example code

```javascript
function function_name(a, b)
{
    console.log(a+b);
}

var array = [1, 4];

function_name(...array); //is equal to function_name(1, 4)
```

###Python like Multiline Strings
One of the most loved python language features is multiline strings. In JavaScript there is actually no support for multiline strings. But by doing some tricks we can get this feature. In this post I will show you three different ways to get this feature in JavaScript.

#####What is Python Multiline Strings

Python multiline string(or block string) is a string which is spread over multiple lines and also new line characters are preserved. A python multiline string looks like this:
```python
string = """
This string is created using triple quotes.
This is a block string.
We are going to achieve this in JavaScript.
"""

print string;
```
#####Multiline string In JavaScript
 There are basically two native methods and one hacked method.

######Method 1
Using the “\” character. Actually “\” character allows us to spread the string over multiple lines but it doesn’t preserve new line characters. Therefore if you are using this method then remember to put new line characters manually. So we can say “\” allows us to create line concatenated string.

```javascript
var string = "This is first line \n\
This is second line \n\
This is third line \n\
";
console.log(string);
```
######Method 2
We can use ECMAScript 6 Template String to achieve the same type of multiline string as python.

```javascript
var name = "narayan";
var x = `
My Name is ${name}.
My Profession is web development.
`;
console.log(x);
```
Many browsers don’t yet support ECMAScript 6 therefore the above example may not work.
ECMAScript 6 template string also support string substitution feature as shown in the above example.

######Method 3
This is basically a hacked way of getting this feature. This method converts JavaScript comments into multiline string preserving the new line characters.

Let’s see an example of this:
```javascript
var myString = (function () {/*
   I am narayan
   I am a web developer
*/}).toString().match(/[^]*\/\*([^]*)\*\/\}$/)[1];

console.log(myString);
```

You can also achieve string substitution using more advanced regular expression functionality.

This is my favorite method and is very compatible. I don’t use this feature from scratch instead I use a library called as JavaScript multiline which provides the same.

One major problem with this method is JS minifiers remove javascript comments. But you can stop them from removing these special comments by this way:

- Uglify: Use /*@preserve instead of /* and enable the comments option
- Closure Compiler: Use /*@preserve instead of /*
- YUI Compressor: Use /*! instead of /*

###"class" Keyword
ECMAScript 6 introduced “class” keyword to define classes in JavaScript. Earlier to ES6, we had to use constructor function.

Here is an example code on how to define classes and then how to create objects of those class types i.e., instances of classes.
```javascript
class Student
{
    //constructor of the class
    constructor(name, age)
    {
        //"this" points to the current object
        this.name = name;

        this._age = age;
    }

    //member function
    getName()
    {
        return this.name;
    }

    setName(name)
    {
        this.name = name;
    }

    //getters and setters make a function accessible like a variable. They are used as wrappers around other variables.
    set age(value)
    {
        this._age = value;
    }

    get age()
    {
        return this._age;
    }
}

//class person inherits student class
class Person extends Student
{
    constructor(name, age, citizen)
    {
        //this points to the person class
        this.citizen = citizen;

        //call constructor of super class. "super" is an pointer to the super class object
        super(name, age);
    }

    getCitizen()
    {
        return this.citizen;
    }

    //overriding
    getName()
    {
        //we are calling the super class getName function
        return super.getName();
    }
}

//instance of student class
var stud = new Student("Narayan", 21);

//instance of person class
var p = new Person("Narayan Prusty", 21, "India");

stud.age = 12; //executes setter
console.log(stud.age); //executes getter
```

###Arrow “=>” Function

ECMAScript 6 provides a new way to create functions which just contain one line of statement. This new type of function is called lambda or arrow function.

Here is how to create a arrow function

```javascript
//sum is the function name
//x and y are function parameters
var sum = (x, y) => x + y;

console.log(sum(2, 900)); //902
```
Here (x, y) => x + y returns a regular JavaScript function object. Here the function body of the returned function object’s body would be function(x, y){return x+ y;}

Arrow functions always return the value of the statement when executed. Here result of x+y is returned.

You can also write multiple statements in an arrow function but arrow functions are mostly used in replacement of single statement functions. Here is code example of multiple statements in an arrow function

```javascript
var sum = (x, y) => {
    x = x + 10;
    y = y + 10;
    return x + y;
}

console.log(sum(10, 10)); //40
```

As arrow function actually returns a regular JavaScript function object so they can be used wherever we use regular JavaScript function object. For example, they can be used as callback.

```javascript
function sum(p, q)
{
    console.log(p() + q()); //87
}

sum(a => 20 + 10, b => 1 + 56); //here we are passing two function objects
```

One last and most important feature about arrow function is that the “this” pointer inside an asynchronously executed arrow function points to the scope inside which it was passed as callback. A regular function’s this pointer points to global scope when executed asynchronously.

```javascript
window.age = 12;

function Person(){
  this.age = 34;

  setTimeout(() => {
    console.log(this.age); //34
  }, 1000);

  setTimeout(function(){
    console.log(this.age); //12
  }, 1000);  
}

var p = new Person();
```
###"for of" Loop
for of loop was introduced in ES6 which allows you to easily iterate over elements of an collection.for of iterates over the values of elements of a collection not the keys. A collection can be an array, set, list, custom collection object etc.

Earlier to ES6 we had to use for loop or Array’s foreach loop to walkthrough elements of an collection. ES6 introduced a new way for iteration.

An iterator is an construct that lets us visit or walkthrough every element of an collection.

#####Iterating an Array using “for of”
Here is an example code on how to iterate an array using for of loop
```javascript
var array = [1, 3, 5, 7, 9];

//'i' references to the values of the array indexes
for(var i of array)
{
    console.log(i); //1, 3, 5, 7, 9
}
```
Internally for of loop uses @@iterator method of an collection object i.e., for a collection object to be iterable using for of it must have property with a Symbol.iterator key.

#####Iterating an Custom Collection Object using “for of”
We need to implement Symbol.iterator property on an custom collection. Symbol.iterator returns a Iterator object i.e., a object with next() property. next() is invoked by for of until next() returns {value: undefined, done: true}. To continue looping and return an collection element next() has to return {value: element_value, done: false}.

Here is an code example:
```javascript
var custom_collection = {
    elements:  [1, 4, 6, 9],
    size : 3,
    pointer :0,
    [Symbol.iterator]:  function(){
        var e = this.elements;
        var s = this.size;
        var p = this.pointer;
        return{
            next: function() {
                if(p > s)
                {
                    return { value: undefined, done: true };
                }
                else
                {
                    p++;
                    return { value: e[p - 1], done: false };
                }
            },
        };
    }
}

for(var i of custom_collection)
{
    console.log(i); //1, 4, 6, 9
}
```
###"yield" Keyword and "function*()" Syntax
ECMAScript 6 specification introduced a new JavaScript feature called as JavaScript Generators. JavaScript’s yield keyword and function*() syntax together make JS Generators.

In nutshell JavaScript generators provide a new way for functions to return a collection and also a new way of looping(or iterating) through the elements of the returned collection.

Earlier to JavaScript Generators you would do something like this:
```javascript
function collection_name()
{
    return [1, 3, 5, 7];
}

var collection = collection_name();

for(var iii = 0; iii < collection.length; iii++)
{
    console.log(collection[iii]);
}
```
Here is how you can do the same using Generators
```javascript
function* collection_name()
{
    yield 1;
    yield 3;
    yield 5;
    yield 7;
}

for(var iii  of collection_name())
{
    console.log(iii);
}
```
Internally JavaScript creates a object with Symbol.iterator property from the yielded values which is what for of construct needs for iterating a collection.

###"0o" Literal
“0o” is a new way of creating a number using octal value in ES6. Earlier to ES6, JS developers had to use “0” in front of numbers to specify its a octal number.

JavaScript internally converts hexadecimal and octal numbers to decimal and then stores them as binary.

Here is code example:
```javascript
//Before ES6
var a = 012;
console.log(a); //10

//ES6
var b = 0o12;
console.log(b); //10
```

So in ES6 its more easier to identify octal numbers.

###"0b" Literal
“0b” lets you create a number using binary of the number directly. Developers usually provide number in decimal representation and it was converted to binary and stored in memory but from ES6 onwards you can specify the binary of the number directly.

Here is an example of how to use “0b” prefix literal

```javascript
var a = 0b11011101;
console.log(a); //221
```

###"Set" Object
JavaScript “Set” object is a collection of unique keys. Keys are object references or primitive types.

Arrays can store duplicate values but Sets don’t store duplicate keys, this is what makes it different from arrays.

Here is code example on how to create a set object, add keys, delete keys, find size etc.

```javascript
//create a set
var set = new Set();

//add three keys to the set
set.add({x: 12});
set.add(44);
set.add("text");

//check if a provided key is present
console.log(set.has("text"));

//delete a key
set.delete(44);

//loop through the keys in an set
for(var i of set)
{
    console.log(i);
}

//create a set from array values
var set_1 = new Set([1, 2, 3, 4, 5]);

//size of set
console.log(set_1.size); //5

//create a clone of another set
var set_2 = new Set(set.values());
```

###Difference between Set and WeakSet in JavaScript
JavaScript Set and WeakSet objects allows you to store collection of unique keys. But there are some key differences between them. Here are the differences:

- They both behave differently when a object referenced by their keys gets deleted. Lets take the below example code:

```javascript
var set = new Set();
var weakset = new WeakSet();

(function(){
    var a = {x: 12};
    var b = {y: 12};

    set.add(a);
    weakset.add(b);
})()
```

One the above self invoked function is executed there is no way we can reference {x: 12} and {y: 12} anymore. Garbage collector goes ahead and deletes the key b pointer from “WeakSet” and also removes {y: 12} from memory. But in case of “Set”, the garbage collector doesn’t remove a pointer from “Set” and also doesn’t remove {x: 12} from memory.

So “Set” can cause more garbages in memory. We can say that “Set” references are strong pointer whereas “WeakSet” references are weak pointers.

- “WeakSet” keys cannot be primitive types. Nor they can be created by an array or another set.

```javascript
var set = new Set([1,2,3,4]);

//cannot be created from array or another set
var weakset = new WeakSet();
weakset.add({a: 1}); //object reference must
```
- “WeakSet” doesn’t provide any methods or functions to work with the whole set of keys. For example: size, looping etc.

```javascript
var set = new Set([1,2,3,4]);

//cannot be created from array
var weakset = new WeakSet();
weakset.add({a: 1}); //object reference must

console.log(set.size);//4
console.log(weakset.size);//undefined

for(var i of set)
{
    console.log(i); //1,2.3.4
}

//doesn't execute throws error
for(var i of weakset)
{
    console.log(i);
}

set.clear();
weakset.clear(); //This works
```

###"Map" Object
JavaScript “Map” object is a collection of unique keys and corresponding values. Keys and Values can be object references or primitive types.

2D Arrays can store duplicate values but Maps don’t store duplicate keys, this is what makes it different from 2D arrays.

A JavaScript “Set” object can store only keys but “Map” can store key and value pairs.

Here is code example on how to create a Map object, add keys, delete keys, find size etc.

```javascript
//create a map
var map = new Map();

//add three keys & values to the map
map.set({x: 12}, 12);

//same key is overwritten
map.set(44, 13);
map.set(44, 12);

//check if a provided key is present
console.log(map.has(44)); //true

//retrieve key
console.log(map.get(44)); //12

//delete a key
map.delete(44);

//loop through the keys in an map
for(var i of map)
{
    console.log(i);
}

//delete all keys
map.clear();

//create a map from arrays
var map_1 = new Map([[1, 2], [4, 5]]);

//size of map
console.log(map_1.size); //2
```
###Difference between "Map" and "WeakMap"

JavaScript “Map” and “WeakMap” objects allows you to store collection of unique keys and their corresponding values. But there are some key differences between them. Here are the differences:

- They both behave differently when a object referenced by their keys/values gets deleted. Lets take the below example code:

```javascript
var map = new Map();
var weakmap = new WeakMap();

(function(){
    var a = {x: 12};
    var b = {y: 12};

    map.set(a, 1);
    weakmap.set(b, 2);
})()
```
One the above self invoked function is executed there is no way we can reference {x: 12} and {y: 12} anymore. Garbage collector goes ahead and deletes the key b pointer from “WeakMap” and also removes {y: 12} from memory. But in case of “Map”, the garbage collector doesn’t remove a pointer from “Map” and also doesn’t remove {x: 12} from memory.

So “Map” can cause more garbages in memory. We can say that “Map” references are strong pointer whereas “WeakMap” references are weak pointers.

- “WeakMap” keys cannot be primitive types. Nor they can be created by an 2D array.

```javascript
map.set(44, 12);

//throws invalid type error
weakmap.set(44, 13);

//doesn't work. throws errors
var map_1 = new WeakMap([[1, 2], [4, 5]]);
```

- “WeakMap” doesn’t provide any methods or functions to work with the whole set of keys. For example: size, looping etc.

```javascript
console.log(weakmap.size); //undefined


//loop through the keys in an map
for(var i of map)
{
    console.log(i);
}

//loop through the keys in an weakmap doesn't work
for(var i of weakmap)
{
    console.log(i);
}

//delete all keys
map.clear();

weakmap.clear(); //but this works
```

###"then" Function
JavaScript then() is a property of JavaScript “Promise” object. JavaScript “Promise” object lets you execute/call a function asynchronously. Promises were introduced in ECMAScript 6.

There are many ways to call a JavaScript function asynchronously. But there was no predefined way of doing it. Many use setTimeout() to execute a function asynchronously. Here is code example on how to execute a function asynchronously using setTimeout()
```javascript
function y1()
{
    console.log("Callback fired");
}

setTimeout(y1, 0);
```

Here is how we can do the same using ES6 “Promise” object and its then() function.

```javascript
function y1(resolve, reject)
{
  console.log("Callback fired");
  resolve();
}  

//y1 is referenced by "then" property and is executed asynchronously.
var promise = new Promise(y1);

function success()
{
    console.log("Successful");
}
 
function error(e)
{
    console.log(e);
}

promise.then(success, error);
```

While creating “Promise” object we have to provide then() function’s implementation. The then() function is executed asynchronously and so the passed callbacks. We need to pass two callbacks to the then() function i.e., resolve and reject callbacks. resolve callback should be fired if the asynchronous operation was successful otherwise reject callback should be fired with an error object.

then() always returns a promise object. The implementation of this promise object depends on the following

- When then() is successful(i.e., executes resolve callback) and has no return statement then it internally creates and returns a new promise object whose then() fires resolve callback.
- When then() is successful(i.e., executes resolve callback) and if we return a promise inside then() function’s resolve callback then then() function returns that particular promise instead of creating one internally.
- When then() is successful(i.e., executes resolve callback) and if you return something else other than an custom promise object inside then() function’s resolve callback then then() function internally creates the default promise and uses the other type of returned values as input to the resolve of new default promise. then() returns this internally created promise.
- When then() is successful(i.e., executes resolve callback) and if you return a non promise type inside then() function then its ignored. In this case then() internally creates and returns a new promise object whose then() fires resolve callback.
- When then() function fails(i.e, fires reject callback) and has no return statement then a new promise is created internally and is returned by the then() function. This new promise’s then() function fires resolve callback.
- When then() function fails(i.e, fires reject callback) and the reject callback returns a custom promise then then() returns that particular promise.
- When then() fails(i.e., executes reject callback) and if you return something else other than an custom promise object inside then() function’s reject callback then then() function internally creates the default promise and uses the other type of returned values as input to the resolve of new default promise. then() returns this internally created promise.
- When then() fails(i.e., executes reject callback) and if you return a non promise type inside then() function then its ignored. In this case then() internally creates and returns a new promise object whose then() fires resolve callback.

Here are couple of examples to understand the above mentioned points.

```javascript
/*
Output:

2
1
*/

var promise = new Promise(function(resolve, reject){
    resolve(2);

    //ignored
    return 6;
});

function error(e)
{
    console.log(e);
}

function success(value)
{
    console.log(value);
    return 1;
}

promise.then(success, error).then(success, error);
```
```javascript
/*
Output:

Error
Success
*/

var promise = new Promise(function(resolve, reject){
    reject("Error");
});

function error(e)
{
    console.log(e);
}

function success()
{
    console.log("Success");
}

promise.then(success, error).then(success, error);
```

###Promise "catch" Function
JavaScript’s Promise object is used wrap asynchronous operation and its events. Whenever we run asynchronous code there is chance of implicit or explicit exception. We can obviously catch exceptions using try...catch but Promise provide a more appropriate way of handling exception.

Here is an example on how to handle exception inside then() function using try...catch

```javascript
/*
Output:

Exception  
*/
 
var promise = new Promise(function(resolve, reject){
    try
    {
        throw "Exception";
        resolve();
    }
    catch(e)
    {
        console.log(e);
    }
});

function success()
{
    console.log("Successful");
}
 
function error(e)
{
    console.log(e);
}

promise.then(success, error);
```

Although this works great but Promise provides a new way to catch exceptions

```javascript
/*
Output:

Exception  
*/
 
var promise = new Promise(function(resolve, reject){
    throw "Exception";
    resolve();
});

function success()
{
    console.log("Successful");
}
 
function error(e)
{
    console.log(e);
}

function catched(e)
{
    console.log(e);
}

promise.then(success, error).catch(catched);
```

When an exception occurs and there is no catch() function or try...catch then then() uses the reject callback to handle the exception. And then then() internally creates and returns a new promise object whose then() fires resolve callback. But if reject returns a custom promise then that particular promise is returned by then(). If there is also no reject callback also then the promise(or promise chain) is stopped. Here is code example:

```javascript
/*
Output:

Exception
Successful
*/

var promise = new Promise(function(resolve, reject){
    throw "Exception";
    resolve();
});

function success()
{
    console.log("Successful");
}
 
function error(e)
{
    console.log(e);
}

promise.then(success, error).then(success, error);
```

catch() always returns a promise object. You can return a custom promise inside catch() or else catch() internally creates and returns a new promise whose then() fires resolve callback. If you return anything else instead of a promise object inside catch() that thats completely ignored and catch() returns the internally created promise. Here is code example

```javascript
/*
Output:

Exception
Successful
*/
var promise = new Promise(function(resolve, reject){
    throw "Exception";
    resolve();
});

function success()
{
    console.log("Successful");
}
 
function error(e)
{
    console.log(e);
}

function catched(e)
{
    console.log(e);
}
promise.then(success, error).catch(catched).then(success, error);
```

[ES6 fiddle]:http://www.es6fiddle.net/